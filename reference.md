#WeituotianVideoAndroidApp

- 学会Retrofit+OkHttp+RxAndroid三剑客的使用，让自己紧跟Android潮流的步伐
- http://blog.csdn.net/Iamzgx/article/details/51607387/
- Bring RxLifecycle to presenter world
- https://blog.thefuntasty.com/bring-rxlifecycle-to-presenter-world-703d0da5d6d1#.od8als3bh
- 使用Retrofit2.0上传文件，可以监听上传进度[客户端+服务器端代码]
- http://blog.csdn.net/u013762572/article/details/51824645

## OptionMenu
- Android Quick Tips #8 — How to Dynamically Tint Actionbar Menu Icons(动态给菜单图标着色)
- https://futurestud.io/tutorials/android-quick-tips-8-how-to-dynamically-tint-actionbar-menu-icons
- 用Android自带的DrawerLayout和ActionBarDrawerToggle实现侧滑效果
- http://blog.csdn.net/miyuexingchen/article/details/52232751

## greendao
- 关于sd卡中storage/emulated/0找不到问题
- http://blog.csdn.net/sinat_29962405/article/details/50859693
- How to let greenDAO to create the Database on sdcard ? - Android
- http://stackoverflow.com/questions/35198262/how-to-let-greendao-to-create-the-database-on-sdcard-android
- https://github.com/greenrobot/greenDAO/issues/300
- greenDAO数据库文件的路径
- http://blog.csdn.net/u012532559/article/details/53492464
- http://www.jianshu.com/p/82bbdcd75cad
