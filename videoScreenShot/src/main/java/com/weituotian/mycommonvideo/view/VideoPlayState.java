package com.weituotian.mycommonvideo.view;

/**
 * 视频播放枚举
 * Created by ange on 2016/12/27.
 */

public enum VideoPlayState {
    STAND_TO_PREPARE,//静止,没有视频
    PREPARE,//视频已经打开
    PLAY,//播放
    PAUSE//暂停
}
