package com.weituotian.video.presenter;


import com.weituotian.video.http.DataType;

/**
 * @author: laohu on 2016/10/8
 * @site: http://ittiger.cn
 */
public interface TypePresenter {

    DataType getType();
}
