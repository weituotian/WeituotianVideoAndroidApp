package com.weituotian.video.mvpview;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.weituotian.video.entity.User;

/**
 * Created by ange on 2017/3/16.
 */

public interface ILoginView extends MvpView {

    void showLoginFail(String msg);

    void showLoginSuccess();
}
