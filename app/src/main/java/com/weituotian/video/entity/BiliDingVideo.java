package com.weituotian.video.entity;

import java.util.List;

/**
 * bilibili首页视频的信息
 * Created by ange on 2017/3/13.
 */
public class BiliDingVideo {
    /**
     * list : [{"aid":2274880,"typeid":47,"title":"【APH】黑塔都市的演绎者【op风】","subtitle":"","play":194132,"review":705,"video_review":4232,"favorites":19317,"mid":1793047,"author":"晏晓","description":"sm26092395 by oyuzu さん。翻译苦手，用了手书里的标题，原标题为【手描きAPヘタリア】メカアクＯＰパロ，如果有更好的翻译请告诉我ww。","create":"2015-05-01 11:08","pubdate":"1430449734","pic":"http://i0.hdslb.com/bfs/archive/ddb9cbc370f66f37594ad546de2b4763eb2e73c1.jpg_320x200.jpg","credit":0,"coins":2582,"duration":"1:39"},{"aid":8389401,"typeid":27,"title":"青之驱魔师第二季（京都不净王篇） OP ED","subtitle":"","play":1873,"review":0,"video_review":10,"favorites":29,"mid":19908168,"author":"12X7","description":"自剪，青之驱魔师第二季OP,ED，青之驱魔师OP，ED全集（包含第二季和剧场版）：av8393115","create":"2017-02-04 11:49","pubdate":"1486180165","pic":"http://i1.hdslb.com/bfs/archive/dde44bc5c79097f4723651010f4bcbe3388c65b4.jpg_320x200.jpg","credit":0,"coins":13,"duration":"3:01"},{"aid":6515361,"typeid":24,"title":"【励志/治愈】献给正处于迷茫的大家","subtitle":"","play":449508,"review":9,"video_review":11389,"favorites":53847,"mid":27834625,"author":"为什么你能那么熟练啊","description":"BGM:僕らの手には何もないけど\n这次视频是以我个人角度来说最喜欢的歌来做的,也是这首歌曾经拯救了身处迷茫的我.\n那段时间真的是天昏地暗,不过还好的是,我挺了过来,所以说这个视频的初衷就是这么一段话\n&amp;amp;quot;就在你身处迷茫之时,请不要忘记,我还在你身边.&amp;amp;quot;\n这个你可以代表很多,亲人、爱人、家人等等等等.\n无论在什么时间,总会有这么一个人在支持着你,就算你说没有,那up主也支持着你.\n希望身处迷茫你,快点走出困境,然后朝着自己的目标前进着.\n","create":"2016-10-03 15:42","pubdate":"1475480558","pic":"http://i1.hdslb.com/bfs/archive/4ee6cabca20d71d42bf3c883de9c0fa6d7709a99.jpg_320x200.jpg","credit":0,"coins":36425,"duration":"4:07"},{"aid":9090171,"typeid":47,"title":"【手绘op】小林家的妹抖龙【灵魂作画】","subtitle":"","play":18668,"review":0,"video_review":199,"favorites":1177,"mid":1992514,"author":"纸欣Dream","description":"按照之前的约定我画好了妹抖龙的op咯~次的手绘op算是我的最认真的作品了吧hhh，这次的张数比以往的op都要多喔。感谢兔麦桑（张何）的帮忙啦。那么下次见咯~封面id=61452245。\n欢迎加入纸欣的日常闲聊Q群438035325","create":"2017-03-11 11:27","pubdate":"1489202871","pic":"http://i2.hdslb.com/bfs/archive/5169d873eac4d2d92a5adbb22a460f4acb500441.png_320x200.png","credit":0,"coins":1231,"duration":"3:24"},{"aid":9093842,"typeid":27,"title":"【TOP 10】动画里酷炫的十位女生","subtitle":"","play":49269,"review":0,"video_review":1766,"favorites":2300,"mid":91661940,"author":"我们的征途是漫漫星海","description":"来源：油管 heroin Animeweekly \nhttps://www.youtube.com/watch?v=6rTZRZ61c0o","create":"2017-03-11 14:15","pubdate":"1489212917","pic":"http://i1.hdslb.com/bfs/archive/0487b17e9ce0657abc861ff3995ca90672afd11e.jpg_320x200.jpg","credit":0,"coins":192,"duration":"10:52"},{"aid":9124491,"typeid":27,"title":"【老王】八分钟看完《斗罗大陆》02！唐三入史莱克学院！","subtitle":"","play":14576,"review":0,"video_review":112,"favorites":231,"mid":35281133,"author":"影视狂魔老王","description":"八分钟看完《斗罗大陆》02！唐三入史莱克学院！","create":"2017-03-12 17:38","pubdate":"1489311505","pic":"http://i1.hdslb.com/bfs/archive/c4e706ba1a7dd376275c110edd195a70743de93f.jpg_320x200.jpg","credit":0,"coins":190,"duration":"8:00"},{"aid":3449012,"typeid":47,"title":"我只是来送货的呀！（请带好耳机！）","subtitle":"","play":95028,"review":73,"video_review":3671,"favorites":5920,"mid":13510172,"author":"怒榭灀花","description":"自制 cv\r\n墨千临\r\n矜持\r\n御小雅\r\ned\r\n叶天星","create":"2015-12-25 19:20","pubdate":"1451042416","pic":"http://i1.hdslb.com/bfs/archive/3b7cd9429dcafa1857435ff0e13a5c00635b7845.jpg_320x200.jpg","credit":0,"coins":728,"duration":"20:08"},{"aid":8604056,"typeid":47,"title":"【王者荣耀手书】农药全员的Freaks","subtitle":"","play":44021,"review":2,"video_review":1362,"favorites":4964,"mid":2075344,"author":"淘了个淘","description":"BGM：Timmy Trumpet-Freaks\n结尾乱入BGM：なんかのさなぎ\n喜欢魔性的封面hhh，这次的tag比较神奇啊...\n正好在情人节完成手书那么祝大家情人节...斜眼笑\n开学前的垂死挣扎献上，比较潦草抱歉，时间开始紧张了_(:зゝ∠)_","create":"2017-02-14 09:35","pubdate":"1487036104","pic":"http://i2.hdslb.com/bfs/archive/c3f364eed881c200cf95b01148a0811695f8cbbb.png_320x200.png","credit":0,"coins":2089,"duration":"4:38"},{"aid":9092394,"typeid":27,"title":"最受期待国漫新番排行榜","subtitle":"","play":10490,"review":0,"video_review":1335,"favorites":611,"mid":88432099,"author":"中二龙傲地","description":"画质修订版，内容有些改动","create":"2017-03-11 13:11","pubdate":"1489209112","pic":"http://i0.hdslb.com/bfs/archive/8d540a6b9ec9c002e52f71e515f9fe455991ca09.jpg_320x200.jpg","credit":0,"coins":86,"duration":"27:30"},{"aid":8019721,"typeid":24,"title":"【冰上的尤里】法治在线：GAY!!! ON ICE|MAD|OOC","subtitle":"","play":172740,"review":5,"video_review":5827,"favorites":23422,"mid":1795989,"author":"嘉言_","description":"OOC！歌词翻译有部分修改\n被打回了一次，去掉了台标= =\n修改版：更换部分画面、调整部分口型、修改翻译\n\nBGM-there！right there！出自音乐剧《律政俏佳人》\n假酒向第二弹，今天的假酒是关于在YOI法庭审判时深柜大毛做伪证[[？结果当庭被火眼金睛的律师团和人证拆穿的故事\n\n以上均是up主瞎编的。\n","create":"2017-01-15 12:57","pubdate":"1484456232","pic":"http://i2.hdslb.com/bfs/archive/ad6f2fb40c0346d4926cfea32f5c4aa69bc24252.jpg_320x200.jpg","credit":0,"coins":18986,"duration":"3:23"},{"aid":9136048,"typeid":47,"title":"【哪吒传奇/手书】小龙女的心意便当","subtitle":"","play":44,"review":0,"video_review":7,"favorites":0,"mid":2200162,"author":"北光的百岁山","description":"特别喜欢藕龙cp！！以及通宵肝完手书真是。。我今天还要上课的xxx\nbgm：卵とじ\n内容有参考小乔的心意便当\u2014\u2014av7170248","create":"2017-03-13 06:57","pubdate":"1489359458","pic":"http://i1.hdslb.com/bfs/archive/dba0cbe99dcdcc4f707dc0e920663d88ee4764da.jpg_320x200.jpg","credit":0,"coins":14,"duration":"3:59"},{"aid":9119043,"typeid":27,"title":"柯南危险，谁最后注意到灰原，那是只在小兰脸上才有的表情","subtitle":"","play":18345,"review":1,"video_review":232,"favorites":278,"mid":35954237,"author":"星野真夕","description":"http://www.toutiao.com/a6370799873458012417/ 侵删，搬运，由【星野真夕】整理收集，更多新鲜有趣视频，请戳我头像，欢迎关注.","create":"2017-03-12 13:59","pubdate":"1489298387","pic":"http://i2.hdslb.com/bfs/archive/1f0ec2adc5815c2cdfb82eb0535aa18693da056f.jpg_320x200.jpg","credit":0,"coins":21,"duration":"7:47"},{"aid":9082604,"typeid":47,"title":"【氰化物欢乐秀】书 - Book【我们看了那么多的氰化物欢乐秀有什么卵用？这集告诉你答案。】","subtitle":"","play":18343,"review":0,"video_review":66,"favorites":133,"mid":11764347,"author":"chenghaopeng","description":"http://explosm.net/shorts/36/book 补缺集，发布于2010.01.20","create":"2017-03-10 22:42","pubdate":"1489156925","pic":"http://i2.hdslb.com/bfs/archive/cda0c049a21aba34f5cb43c9d33e545912470c4f.png_320x200.png","credit":0,"coins":26,"duration":"1:55"},{"aid":4071887,"typeid":24,"title":"【火影/鸣佐】原来你是我最想留住的幸运","subtitle":"","play":52425,"review":263,"video_review":1227,"favorites":3205,"mid":1750332,"author":"gracesting","description":"自制 BGM：田馥甄-小幸运\r\n\r\nup主纯新人 不足之处请指出w\r\n\r\n中间有GN说出现鼬佐情节我来解释一下，其实就是为了对应歌词啊，佐助杀了鼬以后才后知后觉么。不是出现鼬就是鼬佐好么\u2026\u2026\r\n还有没有修改的第一版在2p 看有的弹幕说到更喜欢第一版就搬上来了 虽然我并没有觉得两个有什么太大差别【手动再见 ","create":"2016-03-12 01:06","pubdate":"1457716019","pic":"http://i2.hdslb.com/bfs/archive/15fae265134cfe42edff5119e8eae6e13ce0c2e0.jpg_320x200.jpg","credit":0,"coins":1433,"duration":"4:22"},{"aid":9075239,"typeid":27,"title":"【氰化物欢乐秀】大合集来了，一次看个够！！！@油兔不二字幕组","subtitle":"","play":38170,"review":0,"video_review":558,"favorites":1292,"mid":15967711,"author":"油兔不二字幕组","description":"https://www.youtube.com/watch?v=Ue38v79x8d0 \n字幕组APP及网站 寻求投资方投资合作，如有兴趣，请加QQ群：386225240\n字幕组紧急招募网站UI、前端后端、ios及安卓开发工程师等职位  申请加入QQ群：586570021（注明职位） \n微信公众号：油兔不二 微博：油兔不二字幕组\n油兔不二招募 片源 听译 翻译 时间轴 后期 压制 校正 特效 宣传 小编 美工 上传发布 欢迎人才加入油兔不二招新群 495193781  油兔粉丝群：5946974","create":"2017-03-10 17:26","pubdate":"1489137965","pic":"http://i2.hdslb.com/bfs/archive/3d8f7483f6c747bf0c2abe46d0741d4dcaf6c0d7.jpg_320x200.jpg","credit":0,"coins":152,"duration":"19:02"},{"aid":6526209,"typeid":24,"title":"【秦时明月MAD】若当来世（论秦时与狐妖小红娘的同步率第二弹）","subtitle":"","play":45381,"review":0,"video_review":1236,"favorites":2554,"mid":1924449,"author":"imalicewt","description":"原创视频禁二改禁商用转载请注明出处 (°∀°)ﾉ","create":"2016-10-04 08:50","pubdate":"1475542254","pic":"http://i2.hdslb.com/bfs/archive/72ddea1ebbf1fdb67bd0be62f634bd74a42cf95a.jpg_320x200.jpg","credit":0,"coins":1406,"duration":"4:08"},{"aid":9135465,"typeid":47,"title":"【内嵌字幕】【东方手书】博丽二人 第八话后篇：一切为了幻想乡【Part1】","subtitle":"","play":473,"review":1,"video_review":63,"favorites":122,"mid":456952,"author":"圆规怪兽","description":"sm30794677（授权转载，自制字幕，转载请标明原作者） 作者：ロックハート 这个动画是东方二次同人《博丽二人》漫画的动画化作品 作者P站：http://www.pixiv.net/member.php?id=12239 这是本系列的第八话后篇\n\nPS：因为这话时间过长将分为2个PART，这里是Part1。","create":"2017-03-13 03:36","pubdate":"1489347370","pic":"http://i1.hdslb.com/bfs/archive/b18a7ed7bc052a7ced3a065707c991a2ec5cbb9e.jpg_320x200.jpg","credit":0,"coins":191,"duration":"20:30"},{"aid":6428078,"typeid":27,"title":"【木鱼微剧场】几分钟看完《未闻花名》","subtitle":"","play":233993,"review":13,"video_review":2501,"favorites":5580,"mid":927587,"author":"木鱼水心","description":"终于，在微光映照的晨曦，面码终于被众人看见，散去，的时候，众人知道了，面码对于自己是喜爱的，从未怪罪过的，怀着美好的心情的，也终于自己原谅了自己，于是，前所未有的畅快的痛哭，然后泪流满面。其实这个时候的流泪，才是真正接受了故人离去的悲伤，而只有痛哭过，才能继续前行。","create":"2016-09-26 20:03","pubdate":"1474891401","pic":"http://i1.hdslb.com/bfs/archive/d0d2f044f9c639104784ff6358c14a758ac745d9.jpg_320x200.jpg","credit":0,"coins":7474,"duration":"14:16"},{"aid":9067918,"typeid":27,"title":"【银翼漫游】日本动漫的审美变迁（五）","subtitle":"","play":883,"review":0,"video_review":115,"favorites":68,"mid":37744278,"author":"银翼大猫","description":"1980年代日本动漫迎来了一个繁荣期","create":"2017-03-10 09:57","pubdate":"1489111020","pic":"http://i2.hdslb.com/bfs/archive/853d7cd8514c61a47d16b1bd515b1c7b3100ce0b.jpg_320x200.jpg","credit":0,"coins":232,"duration":"19:19"},{"aid":9112864,"typeid":24,"title":"【綜漫/燃向/AMV】这是一次圣杯战争召唤！This is a call to arms'！","subtitle":"","play":4460,"review":0,"video_review":140,"favorites":561,"mid":93636265,"author":"HigHerMInd合作社","description":"BGM：This Is Gonna Hurt-Sixx: A.M.\nMEP参与人员\n策划 偷渡到非洲の軒\nstaff&amp;amp;1p cpufan；2p 爱啃肉的栢坂；3p HOLMES--L；4p 干煸豌豆射手；5p 小花生sex；6p 二木十三；7p  newseahonest；8p 十面埋伏_;字幕 探骊得个萝莉\n","create":"2017-03-12 09:01","pubdate":"1489280477","pic":"http://i0.hdslb.com/bfs/archive/ce17172e03628b90c471478bf02456d5e810ee1c.jpg_320x200.jpg","credit":0,"coins":120,"duration":"6:17"}]
     * results : 20
     * pages : 0
     * code : 0
     */

    private int results;
    private int pages;
    private int code;
    private List<VideoBean> list;

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<VideoBean> getList() {
        return list;
    }

    public void setList(List<VideoBean> list) {
        this.list = list;
    }

    public static class VideoBean {
        /**
         * aid : 2274880
         * typeid : 47
         * title : 【APH】黑塔都市的演绎者【op风】
         * subtitle :
         * play : 194132
         * review : 705
         * video_review : 4232
         * favorites : 19317
         * mid : 1793047
         * author : 晏晓
         * description : sm26092395 by oyuzu さん。翻译苦手，用了手书里的标题，原标题为【手描きAPヘタリア】メカアクＯＰパロ，如果有更好的翻译请告诉我ww。
         * create : 2015-05-01 11:08
         * pubdate : 1430449734
         * pic : http://i0.hdslb.com/bfs/archive/ddb9cbc370f66f37594ad546de2b4763eb2e73c1.jpg_320x200.jpg
         * credit : 0
         * coins : 2582
         * duration : 1:39
         */

        private int aid;
        private int typeid;
        private String title;
        private String subtitle;
        private String play;
        private int review;
        private int video_review;
        private int favorites;
        private int mid;
        private String author;
        private String description;
        private String create;
        private String pubdate;
        private String pic;
        private int credit;
        private int coins;
        private String duration;

        public int getAid() {
            return aid;
        }

        public void setAid(int aid) {
            this.aid = aid;
        }

        public int getTypeid() {
            return typeid;
        }

        public void setTypeid(int typeid) {
            this.typeid = typeid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getPlay() {
            return play;
        }

        public void setPlay(String play) {
            this.play = play;
        }

        public int getReview() {
            return review;
        }

        public void setReview(int review) {
            this.review = review;
        }

        public int getVideo_review() {
            return video_review;
        }

        public void setVideo_review(int video_review) {
            this.video_review = video_review;
        }

        public int getFavorites() {
            return favorites;
        }

        public void setFavorites(int favorites) {
            this.favorites = favorites;
        }

        public int getMid() {
            return mid;
        }

        public void setMid(int mid) {
            this.mid = mid;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreate() {
            return create;
        }

        public void setCreate(String create) {
            this.create = create;
        }

        public String getPubdate() {
            return pubdate;
        }

        public void setPubdate(String pubdate) {
            this.pubdate = pubdate;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public int getCredit() {
            return credit;
        }

        public void setCredit(int credit) {
            this.credit = credit;
        }

        public int getCoins() {
            return coins;
        }

        public void setCoins(int coins) {
            this.coins = coins;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }
    }
}
