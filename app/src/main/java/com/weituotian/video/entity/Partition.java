package com.weituotian.video.entity;

/**
 * 视频分区
 * Created by ange on 2017/3/5.
 */
public class Partition {

    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
